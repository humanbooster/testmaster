package ProjetTest2.ProjetTest2;

import static org.junit.Assert.*;

import java.awt.Point;
import java.util.ArrayList;

import org.junit.Test;

public class EnnemiTest {
	Joueur ennemi = new Ennemi();

	@Test
	public void testNbrBateau() {
		assertEquals("Nombre bateaux ", 5,ennemi.getNbrBateau());
	}
	@Test
	public void testIsHitFalse() {
		assertEquals("Appel IsHit() ", false,ennemi.isHit(new Point(5, 5)));

	}
	@Test
	public void testIsDeadFalse() {
		assertEquals("Appel IsDead()",false,ennemi.isDead());

	}
	@Test
	public void testIsHitTrue() {
		assertEquals("Appel IsHit() ", true,ennemi.isHit(new Point(4, 4)));

	}
	@Test
	public void testIsDeadTrue() {
		assertEquals("Appel IsDead()",true,ennemi.isDead());
	}
	
	@Test
	public void testIntersect() {
	assertEquals("Appel Intersect ()", false, ennemi.intersect(new Bateau()));
	}
	
	@Test
	public void testNomEnnemi() {
	assertEquals("Appel getName()","ENNEMI", ennemi.getName());
	}
	
	@Test(timeout=100)
	public void testAffichePosition(){
		ennemi.afficherPosition();
	}
	
	@Test(timeout=100)
	public void testInitialize(){
		ennemi.initialize();
	}

}
