package ProjetTest2.ProjetTest2;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Random;


public class Bateau {
	private ArrayList<Point> mesPosition =new ArrayList<Point>();
	private int length=3;
	
	private String name="";
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Bateau(){
		this.setRandomPosition();
	}

	public ArrayList<Point> getMesPosition() {
		return mesPosition;
	}

	public void setMesPosition(ArrayList<Point> mesPosition) {
		this.mesPosition = mesPosition;
	}
	
	public String toString(){
		String retour = this.name+", Longueur "+this.length+" ";
		return retour;
		
	}

	
	public void setRandomPosition(){
		Point p= new Point();
		int direction;
		java.util.Random gen= new Random();
		direction = gen.nextInt(2);//0 : Horizontal   1 : Vertical 
		p.setLocation(gen.nextInt(10), gen.nextInt(10));
		if(p.x==0) p.x++;
		if(p.y==0) p.y++;
		if(direction==0){
			if(p.x +length <=10){
			for(int i=0;i< length;i++){
				mesPosition.add(new Point(p.x, p.y));
				p.x += 1;}}
			else{for(int i=0;i< length;i++){
				mesPosition.add(new Point(p.x, p.y));
				p.x -= 1;}}}
		else{
			if(p.y +length <=10){
			for(int i=0;i< length;i++){
				mesPosition.add(new Point(p.x, p.y));
				p.y += 1;}}
			else{for(int i=0;i< length;i++){
				mesPosition.add(new Point(p.x, p.y));
				p.y -= 1;}}}
		}
    
	public void affichePosition(){
		int X,Y;
		for(int i =0; i<mesPosition.size();i++){
			X=mesPosition.get(i).x;
			Y=mesPosition.get(i).y;
			System.out.println("x : "+X+" y : "+Y);
		}
	}
	
	public Boolean isHit(Point p){
		if(mesPosition.contains(p)){
			mesPosition.remove(p);
			length--;
			return true;
		}return false;
	}

	public Boolean isDead(){
		if(length== 0) return true;
		else return false;
	}
}
	

